
export function routerConfig($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider) {
  $stateProvider
    .state('example', {
      url: '/example',
      templateUrl: '/example/example.html',
      controller: 'ExampleController'
    })

  $urlRouterProvider.otherwise('/example');
}
