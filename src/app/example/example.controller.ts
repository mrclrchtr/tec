import IScope = angular.IScope;
export class ExampleController {

  public controllerName: String;

  scope: IExampleScope;

  constructor ($scope : IExampleScope) {
    this.scope     = $scope;
    this.scope.controllerName = 'ExampleController';
  }

}

interface IExampleScope extends ng.IScope
{
  controllerName: String;
}
