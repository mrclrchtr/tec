import IDirective = angular.IDirective;
import {ITemplateService} from "../service/template.service";
import {TemplateContent} from "../service/template.service";
import IScope = angular.IScope;

interface ITemplateDirective extends IDirective {

}

export interface IMyScope extends IScope
{
    name: string;
}

export class TemplateDirective implements ITemplateDirective
{

    public link: (scope: IMyScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) => void;
    public template = '<div>{{name}}</div>';
    public scope: IMyScope;

    private templateContent: TemplateContent;

    constructor(TemplateService: ITemplateService) {
        this.templateContent = TemplateService.getTemplateContent();
        this.scope.name = 'Aaron';
    }

}