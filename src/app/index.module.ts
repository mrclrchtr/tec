/// <reference path="../../.tmp/typings/tsd.d.ts" />

import angular = require('angular')
import ngAnimate = require('angular-animate')
import ngCookies = require('angular-cookies')
import ngSanitize = require('angular-sanitize')
import uiRouter = require('angular-ui-router')

[ngAnimate, ngCookies, ngSanitize, uiRouter]

import '../../node_modules/angular-messages';
import '../../node_modules/angular-aria';
import '../../node_modules/angular-touch';
import '../../node_modules/angular-resource';
import '../../node_modules/angular-ui-bootstrap';
import '../../node_modules/angular-toastr';

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { ExampleController } from './example/example.controller';
import { TemplateDirective } from './common/directive/container.directive';
import { TemplateService } from './common/service/template.service';


/** @ngInject */
module app {
  'use strict';

  angular.module('app', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr'])
      .config(config)
      .config(routerConfig)
      .run(runBlock)
      .service('TemplateService', TemplateService)
      .directive(TemplateDirective)
      .controller('ExampleController', ExampleController);
}
