var gulp = require('gulp'),
    typescript = require('gulp-typescript'),
    sourcemaps = require('gulp-sourcemaps'),
    sync = require('gulp-sync')(gulp),
    del = require('del'),
    path = require('path'),
    concat = require('gulp-concat'),
    browserify  = require('browserify'),
    tsify = require('tsify'),
    source = require('vinyl-source-stream'),
    tslint = require('gulp-tslint'),
    watch = require('gulp-watch'),
    watchify = require('watchify'),
    util = require('gulp-util'),
    browserSync = require('browser-sync').create();
    $ = require("gulp-load-plugins")();

/*
 * Build Configuration
 */
var cfg = {

    /*
     * Project Structure
     */
    project: {
        app: {
            base:
            {
                dir: __dirname + '/src/app',
                file: __dirname + '/src/app/index.module.ts'
            }
        },
        assets: {
            base: __dirname + 'src/assets'
        },
        ts: {
          index: __dirname + '/.tmp/typings/tsd.d.ts'
        },
        dest: __dirname + '/target',
        output: "App.js"
    },

    /*
     * TypeScript Compiler Options
     */
    typescript: {
        removeComments: false,
        target: 'ES5',
        module: 'system',
        noExternalResolve: false,
        noImplicitAny: false,
        sortOutput: true
    },
    browserify: {
        debug: true,
        basedir: __dirname + '/src/app',
        entries: __dirname + '/src/app/index.module.ts'
    }

};

/** CLEAN **/
gulp.task('clean', function () {
    del.sync(
        path.join(cfg.project.dest, '/**/*')
    );
});

/** HTML **/
function copyHtml() {
    gulp.src(cfg.project.app.base.dir + '/**/*.html')
        .pipe(gulp.dest(cfg.project.dest));
}
gulp.task('copy.html', copyHtml);
gulp.task('copy.html.reload', ['copy.html'], browserSync.reload);



gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: cfg.project.dest
        }
    });
});

function buildScripts(watch) {
    var bundler = browserify(cfg.browserify)
        .plugin(tsify);

    function build() {
        return bundler.bundle()
            .on("error", printError)
            .pipe(source(cfg.project.output))
            //.pipe($.if(!debug, buffer()))
            //.pipe($.if(!debug, $.uglify()))
            .pipe(gulp.dest(cfg.project.dest))
            .pipe(browserSync.reload({ stream: true, once: true }));
    }

    if (!watch)
        return build();

    bundler
        .plugin(watchify)
        .on("update", function () {
            util.log(util.colors.grey("Building scripts..."));
            build();
        })
        .on("time", function (timeMs) {
            util.log(
                util.colors.grey("Finished"),
                util.colors.cyan("'dev.scripts.watch' after"),
                util.colors.magenta(timeMs.toLocaleString() + " ms"));
        });

    return build();

}

gulp.task("dev.scripts", function () {
    return buildScripts(false);
});

gulp.task("dev.scripts.watch", function () {
    return buildScripts(true);
});

gulp.task("dev.watch", ['dev.scripts.watch', 'copy.html']);


/**
 * This task starts browserSync. Allowing refreshes to be called from the gulp
 * bundle task.
 */
gulp.task('browser-sync', ['dev.watch'], function()
{
    browserSync.init({
        server: {
            baseDir: cfg.project.dest
        }
    });

    gulp.watch(cfg.project.app.base.dir + '/**/*.html', ['copy.html.reload'])

});

function printError(err) {
    util.log(util.colors.red.bold(err.type + " " + err.name + ":"), util.colors.white(err.message));
    this.emit("end");
}